﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using MVCEmployees.Models;

namespace MVCEmployees.Controllers
{
    public class HomeController : Controller
    {

        private IMongoCollection<Employee> collection;

        public HomeController()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("FirstDataBase");
            this.collection = db.GetCollection<Employee>("Employees");
        }

        public IActionResult Index()
        {
            var model = collection.Find
            (FilterDefinition<Employee>.Empty).ToList();

            Debug.WriteLine(model);
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Insert(Employee emp)
        {
            collection.InsertOne(emp);
            ViewBag.Message = "Employee added succesfully";

            return RedirectToPage("/Index.cshtml");
           
        }

        public IActionResult Update(string id)
        {
            ObjectId old = new ObjectId(id);
            Employee emp = collection.Find(
                                 e => e.Id == old).FirstOrDefault();
            return View(emp);
        }
       
        [HttpPost]
        public IActionResult Update(string id, Employee emp)
        {
            emp.Id = new ObjectId(id);
            var filter = Builders<Employee>.
                Filter.Eq("Id", emp.Id);
            var updateDef = Builders<Employee>.Update.
                Set("FirstName", emp.FirstName);
            updateDef = updateDef.Set("LastName", emp.LastName);
            var result = collection.UpdateOne(filter, updateDef);

            if (result.IsAcknowledged)
            {
                ViewBag.Message = "Employee update successfully!";
            }

            else
            {
                ViewBag.Message = "Error while updating Employee!";
            }
            return View(emp);
        }
        
    }
}
